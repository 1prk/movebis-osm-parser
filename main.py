import requests
import json
import shapely
from shapely import wkt
from shapely.geometry import Polygon
import pandas as pd
import geopandas as gpd
import folium

def df_chunk(csv, minimum, bbox):

        chunksize = 1000000
        for df in pd.read_csv(csv, skip_blank_lines=True, header=0, sep=',', quotechar='"', error_bad_lines=False, chunksize=chunksize):
            print(df.shape)

            #filter data by occurences
            df = df[df.occurrences > minimum].copy()
            df = df.rename(columns={df.columns[0]: "geometry"})
            df['geometry'] = df['geometry'].apply(wkt.loads)

            #load filtered data into a geodataframe
            gdf = gpd.GeoDataFrame(df, geometry='geometry', crs=4326)

            #clip filtered geodataframe with the bounding box
            gdf_bbox = gpd.clip(gdf, bbox)

            #save geodataframe to a csv file
            gdf_bbox.to_csv("test.csv", index=True, header=None, mode='a')
            print(gdf_bbox)
            if gdf_bbox.empty == False:
                return gdf_bbox

def bbox_city(city):
    url = "https://nominatim.openstreetmap.org/search"
    payload = {'city': city,
               'format': 'json'}

    x = requests.get(url, params = payload)

    js = json.loads(x.text)

    bbox_raw = js[0]['boundingbox']
    bbox = (float(bbox_raw[2]), float(bbox_raw[0]), float(bbox_raw[3]), float(bbox_raw[1]))
    #bbox = (float(bbox_raw[0]), float(bbox_raw[2]), float(bbox_raw[1]), float(bbox_raw[3]))
    polygon = shapely.geometry.box(*bbox)
    poly_gdf = gpd.GeoDataFrame([1], geometry=[polygon], crs=4326)
    return poly_gdf

def map(gdf):
    gjson = gdf.to_json()
    m = folium.Map([51.3,13.2], zoom_start=10)
    folium.GeoJson(gjson).add_to(m)
    m.save('test.html')

#testing function for edges with over 100 occurrences, plotting them on a leaflet map
map(df_chunk('verkehrsmengen_2020.csv', 300, bbox_city('Leipzig')))
